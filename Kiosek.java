import java.util.*;

public class Kiosek implements IKiosek {
    public static final int VELIKOST_SKLADU = 20;
    private List<IZbozi> skladKiosku;
    private List<IUcet> uctenky;

    public Kiosek() {
        this.skladKiosku = new ArrayList<>();
        this.uctenky = new ArrayList<>();
    }

    /**
     * Metoda vrati celkovou cenu naskladneneho zbozi
     */
    public double cenaNaskladnehoZbozi() {
        double cena = 0;
        for (IZbozi zbozi : skladKiosku) {
            cena += zbozi.getCena();
        }
        return cena;
    }

    /**
     * Metoda vlozi do skladu zbozi (pocet kusu zbozi ve skladu nesmi presahnout VELIKOST_SKLADU)
     * Pokud je na vstupu vic zbozi nez se vejde do skladu, nic se nenaskladni.
     */
    public void naskladni(List<IZbozi> zbozi) {
        if ((skladKiosku.size() + zbozi.size()) <= VELIKOST_SKLADU) {
            skladKiosku.addAll(zbozi);
        }
    }

    /**
     * Metoda prida libovolnou polozku ze skladu a vystavi ucet
     */
    public IUcet necoProdej() {
        if (skladKiosku.size() > 0) {
            IZbozi zbozi = skladKiosku.get(0);
            skladKiosku.remove(0);

            IUcet uctenka = new Ucet(zbozi.getCena());
            uctenky.add(uctenka);
            return uctenka;
        }

        return null;
    }

    /**
     * Metoda zlevni vsechno naskladnene zbozi o pocet procent na vstupu metody
     */
    public void vseZlevni(int sleva) {
        for (IZbozi zbozi : skladKiosku) {
            zbozi.setCena(zbozi.getCena() * ((double)sleva / 100));
        }
    }

    /**
     * Metoda spocita cenu prodaneho zbozi (podle evidovanych uctu)
     */
    public double celkovyZisk() {
        double zisk = 0;
        for (IUcet uctenka : uctenky) {
            zisk += uctenka.getCena();
        }
        return zisk;
    }

    /**
     * Metoda vytiskne do konzole ucty (tvar vypisu je: {kodUcetenky} s cenou {cena} )
     */
    public void tiskniUcty() {
        for (IUcet uctenka : uctenky) {
            System.out.println(uctenka.getKodUctenky() + "s cenou " + uctenka.getCena());
        }
    }

    /**
     * Metoda vyprazdni sklad kiosku
     */
    public void vyhodZbozi() {
        this.skladKiosku.clear();
    }

    /**
     * Metoda smaze vsechny ucty
     */
    public void skartujUcty() {
        uctenky = new ArrayList<>();
    }

    public int pocetNaskladnenehoZbozi() {
        return this.skladKiosku.size();
    }

}
