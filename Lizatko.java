import java.text.DecimalFormat;

/**
 * Write a description of class Lizatko here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Lizatko implements IZbozi
{
    private String prichut;
    private double cena;
    
    public Lizatko(String prichut, double cena) {
        this.cena = cena;
        this.prichut = prichut;
    }
    
    public double getCena() {
        return this.cena;
    }
    
    public void setCena(double cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.##");
        return "Lizatko s prichuti " + prichut + " - " + df.format(cena) + " Kc";
    }
}
