import java.text.DecimalFormat;

public class Noviny implements IZbozi
{
    private String nazev;
    private double cena;
    
    public Noviny(String nazev, double cena) {
        this.cena = cena;
        this.nazev = nazev;
    }
    
    public double getCena() {
        return this.cena;
    }
    
    public void setCena(double cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.##");
        return "Noviny " + nazev + " - " + df.format(cena) + " Kc";
    }
}
