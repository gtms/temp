import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ParekVRohliku implements IZbozi
{
    private double cena;
    private Priloha priloha;
    
    public ParekVRohliku(double cena, Priloha priloha) {
        this.cena = cena;
        this.priloha = priloha;
    }
    
    public void setCena(double cena) {
        this.cena = cena;
    }
    
    public double getCena() {
        return this.cena;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.##");
        DecimalFormatSymbols sym = DecimalFormatSymbols.getInstance();
        sym.setDecimalSeparator(',');
        df.setDecimalFormatSymbols(sym);
        return "Parek v rohliku s " + (priloha == Priloha.KECUP ? "kecupem" : "horcici") + " - " + df.format(cena) + " Kc";
    }
}
